﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace CoreCaseManagementSystem.Models.ViewModels
{
    public class EditCaseViewModel
    {
        [Required]
        public string CaseId { get; set; }
        [Required]
        public string CaseDetailId {get;set;}
        [Required(ErrorMessage = "Please enter an Agency")]
        public string Agency { get; set; }
        [Required(ErrorMessage = "Please enter the first name of the Defendant")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please enter the last name of the Defendant")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Please enter a valid street address")]
        public string StreetAddress { get; set; }
        [Required(ErrorMessage = "Please enter a valid State")]
        [StringLength(maximumLength: 2, ErrorMessage = "State can not exceed 2 characters")]
        public string State { get; set; }
        [Required(ErrorMessage = "Please enter a valid city")]
        public string City { get; set; }
        [Required(ErrorMessage = "Please enter a valid Zipcode")]
        [StringLength(maximumLength: 15, ErrorMessage = "State can not be less than or exceed 7 characters")]
        public string ZipCode { get; set; }

        public List<ChargeChecklist> Checklist { get; set; }

    }
}