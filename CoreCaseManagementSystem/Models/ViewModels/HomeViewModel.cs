﻿using System.Collections.Generic;
using CoreCaseManagementSystem.Models.EF;

namespace CoreCaseManagementSystem.Models.ViewModels
{
    public class HomeViewModel
    {
        public List<Case> Cases { get; set; }
        public List<CaseDetail> Details { get; set; }
    }
}