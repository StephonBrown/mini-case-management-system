﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreCaseManagementSystem.Models.EF;

namespace CoreCaseManagementSystem.Models.ViewModels
{
    public class ChargeChecklist
    {
        public Charge Charge { get; set; }
        public bool IsSelected { get; set; }
    }
}