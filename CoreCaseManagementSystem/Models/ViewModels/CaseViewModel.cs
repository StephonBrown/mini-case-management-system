﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreCaseManagementSystem.Models.EF;

namespace CoreCaseManagementSystem.Models.ViewModels
{
    public class CaseViewModel
    {
        public Case Case { get; set; }
        public CaseDetail Details { get; set; }
        public List<Charge> Charges { get; set; }

    }
}