﻿using System;


namespace CoreCaseManagementSystem.Models.EF
{
    public class CaseDetail
    {
        public int CaseDetailId { get; set; }
        public string DefendantFirstName { get; set; }
        public string DefendantLastName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public Case Case { get; set; }
    }
}