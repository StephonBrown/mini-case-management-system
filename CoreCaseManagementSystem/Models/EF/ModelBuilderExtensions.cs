﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace CoreCaseManagementSystem.Models.EF
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            IList<Guid> randomGuids = new List<Guid>();
            for (int i=0; i<3; i++)
            {
                randomGuids.Add(Guid.NewGuid());
            }
            //This method will be called after migrating to the latest version.
            modelBuilder.Entity<Charge>().HasData(
                new Charge { ChargeId = 1,Name = "Arson", Description = "Fire" },
                new Charge { ChargeId = 2,Name = "Aggravated Assault/Battery", Description = "Hitting" },
                new Charge { ChargeId = 3,Name= "Bribery", Description = "Talking" },
                new Charge { ChargeId = 4,Name = "Burglary", Description = "Stealing" }
              );
            modelBuilder.Entity<CaseDetail>().HasData(
                new CaseDetail { CaseDetailId = 1, DefendantFirstName = "Ben", DefendantLastName = "Blond", City = "Canton", State = "OH", StreetAddress = "1012 Flower Road NE", ZipCode = "44673" },
                new CaseDetail { CaseDetailId = 2, DefendantFirstName = "John", DefendantLastName = "Benny", City = "Canton", State = "OH", StreetAddress = "156 Gda Road SE", ZipCode = "44563" },
                new CaseDetail { CaseDetailId = 3, DefendantFirstName = "Tim", DefendantLastName = "Tommid", City = "Canton", State = "OH", StreetAddress = "12 Reed Road NW", ZipCode = "44658" }
              );

            modelBuilder.Entity<Case>().HasData( 
                new Case
                {
                    CaseId = randomGuids[0],
                    FilingDate = DateTime.Today,
                    Agency = "Criminal Court",
                    CaseDetailFK = 1,

                },
                new Case
                {
                    CaseId = randomGuids[1],
                    FilingDate = new DateTime(2008, 3, 1),
                    Agency = "Running Court",
                    CaseDetailFK = 2,

                },
                new Case
                {
                    CaseId = randomGuids[2],
                    FilingDate = new DateTime(2002, 7, 2),
                    Agency = "Filing Court",
                    CaseDetailFK = 3,
                });

            modelBuilder.Entity<Case>()
                .HasMany(c => c.Charges)
                .WithMany(a => a.Cases)
                .UsingEntity(h => h.HasData(
                    new { CasesCaseId = randomGuids[0], ChargesChargeId = 1 },
                    new { CasesCaseId = randomGuids[0], ChargesChargeId = 2 },
                    new { CasesCaseId = randomGuids[1], ChargesChargeId = 2 },
                    new { CasesCaseId = randomGuids[1], ChargesChargeId = 3 },
                    new { CasesCaseId = randomGuids[2], ChargesChargeId = 3 },
                    new { CasesCaseId = randomGuids[2], ChargesChargeId = 4 }
                ));


        }
    }
}
