﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CoreCaseManagementSystem.Models.EF
{
    public class Case
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CaseId { get; set; }
        [ForeignKey("CaseDetail")]
        public int CaseDetailFK { get; set; }
        public CaseDetail CaseDetail { get; set; }
        public DateTime FilingDate { get; set; }
        public string Agency { get; set; }
        public ICollection<Charge> Charges { get; set; }

    }
}