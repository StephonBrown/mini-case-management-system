﻿
using Microsoft.EntityFrameworkCore;

namespace CoreCaseManagementSystem.Models.EF
{
    public class CaseRepository : DbContext
    {
        public CaseRepository(DbContextOptions options): base(options)
        {
        }
        public DbSet<Case> Cases { get; set; }
        public DbSet<CaseDetail> CaseDetails { get; set; }
        public DbSet<Charge> Charges { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
        }
    }


}