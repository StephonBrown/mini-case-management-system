﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreCaseManagementSystem.Models.EF
{
    public class Charge
    {
        public int ChargeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<Case> Cases { get; set; }
    }
}